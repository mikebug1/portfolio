import React from "react";
import styled from "styled-components";
import Project from "./project";
import CurencyV4 from "../assets/currency-v4.png";
import Esteem from "../assets/Esteem.png";
// import EsteemOpening from "../assets/Opening Page.png";
import CustomerProfilePage from "../assets/CustomerProfilePage.png";
import aiCard from "../assets/aicard.png";
import SpacedCards from "../assets/SpacedCard.png";

const Contents = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

Project.defaultProps = [
  {
    name: "Flash Card Web App",
    role: "Lead Designer",
    date: "2024 - Current",
    description:
      "Developed an application that converts videos into note cards and written notes, streamlining the process of capturing important content from video sources. Implemented innovative features to enhance user experience and productivity.",
    tags: [
      "Figma",
      "Next.js",
      "Web Development",
      "User Experience",
      "Productivity",
      "Innovation",
      "User Experience Research",
    ],
    picture: SpacedCards,
    figma: `https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fdesign%2FRPkmLRVmgimxUds8bO72iR%2FSpaced-Card%3Fnode-id%3D0-1%26t%3DqwPmpZ3TS5QFOZVe-1`,
    modalDescription: `In the Spaced Cards project, I served as the UX Designer on a small, focused team of two. My primary responsibilities included conducting user research to understand the needs and preferences of our target audience, ensuring that design decisions were user-centric. I created wireframes and interactive prototypes to visualize and iterate on the layout and functionality of the study cards website, aiming for a smooth and intuitive user experience. Additionally, I focused on designing the user interface with an emphasis on aesthetics, usability, and accessibility, ensuring the website was visually appealing and easy to navigate. I organized and conducted user testing sessions to gather feedback and identify areas for improvement, refining the design based on user input to enhance usability and satisfaction. Throughout the project, I collaborated closely with the developer to ensure the design was implemented accurately and effectively, maintaining a constant feedback loop to address any design or functionality issues. My contributions were pivotal in creating a user-friendly platform that allows users to easily create and manage their study flashcards.`,
  },
  {
    name: "Video-to-Note Converter App",
    role: "Lead Frontend Developer/Designer",
    date: "2022 - 2023",
    description:
      "Developed an application that converts videos into note cards and written notes, streamlining the process of capturing important content from video sources. Implemented innovative features to enhance user experience and productivity.",
    tags: [
      "Figma",
      "Next.js",
      "Web Development",
      "User Experience",
      "Productivity",
      "Innovation",
      "User Experience Research",
    ],
    picture: aiCard,
    figma: `https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fdesign%2FOxmX6Pfx8f0v5MoYEKrHQD%2FFRFR%3Fnode-id%3D0-1%26t%3Dll2cLYWWv9DpzhfS-1`,
    modalDescription: `The application I created is designed to transform videos into both visual note cards and written notes, revolutionizing the way users capture and retain essential information from video content. By incorporating cutting-edge functionalities, I aimed to elevate the user experience and overall productivity.

Through meticulous development, the application seamlessly converts video content into concise note cards. These note cards distill the key concepts, ideas, and information present in the video, ensuring that users can quickly grasp the essential content without having to review the entire video again. This feature significantly accelerates the learning process and enables users to efficiently absorb knowledge.

In addition to the note card feature, the application also generates comprehensive written notes from the video content. The notes are structured in a logical and coherent manner, capturing the main points, examples, and explanations provided in the video. This written format facilitates deeper understanding and serves as a valuable resource for users who prefer traditional note-taking methods.`,
  },
  {
    name: "Customer Servicing Hub",
    role: "Software Engineer",
    date: "2021 - Current",
    description:
      "Established a UI library for standardized UX design across frontend applications. Implemented full stack features in Angular and C# to improve customer experience. Facilitated cross-organizational communications to ensure deliverables met the needs of all stakeholders.",
    tags: [
      "UI Library",
      "Angular",
      "C#",
      "Customer Experience",
      "Communication",
    ],
    picture: CustomerProfilePage,
    modalDescription: `Working on both the frontend and backend aspects of the project allowed me to gain a holistic understanding of the software development process. On the frontend, I utilized Angular to develop responsive and user-friendly interfaces, integrating the components from the UI library seamlessly into various applications. On the backend, I leveraged the power of C# to build robust functionalities that supported the frontend features and facilitated smooth user interactions.

A critical aspect of this project was effective communication and collaboration across different teams and departments. I played a key role in fostering cross-organizational discussions, ensuring that the requirements and expectations of all stakeholders were addressed. This proactive approach helped align the project's objectives with the needs of various teams, resulting in a final product that not only met but exceeded expectations.

In conclusion, my contribution to this project involved establishing a UI library that set forth a unified and user-centric design language across frontend applications. By implementing features using Angular and C#, I aimed to enhance the overall customer experience. Furthermore, my efforts in facilitating communication between different stakeholders were crucial in ensuring the successful delivery of a solution that satisfied the diverse needs of the organization.
    `,
  },
  {
    name: "Esteem App",
    location: "Buffalo, NY",
    role: "Software Engineer",
    date: "2020 - 2022",
    description:
      "Building a social media platform to allow users to interact with each other and build discussions. Developed Figma sketches and React Native frontend for the web, Android, and iOS platforms. Served as the project manager of the application, ensuring that we stayed on the determined velocity until alpha.",
    tags: [
      "Social Media",
      "Figma",
      "React Native",
      "Project Management",
      "Web Development",
      "Android",
      "iOS",
    ],
    figma:
      "https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fdesign%2FHKLM5jaK6Qjy4aSErKvwHr%2FEsteem%3Fnode-id%3D956-5857%26t%3DuQCf0z3g5laEMPGO-1",
    picture: Esteem,
    modalDescription: `In the dynamic realm of digital communication, embarking on the journey to establish a novel social media platform holds the promise of forging connections and igniting conversations across the virtual landscape. This innovative endeavor encompasses a harmonious blend of design, development, and strategic project management, all aimed at creating an interactive online haven where users can engage, interact, and cultivate discussions.
 `,
  },
  {
    name: "Currency Design System V4",
    date: "2019 - 2020",
    role: "UI Engineer",
    description:
      "Worked on digital design efforts to enhance brand identity across platforms. Ensured seamless compatibility on diverse devices and browsers. Developed modern web components using JavaScript, HTML5, and SCSS. Collaborated with stakeholders for efficient UI component distribution.",
    tags: [
      "UI Design",
      "Digital Design",
      "Compatibility",
      "Web Components",
      "JavaScript",
      "HTML5",
      "SCSS",
    ],
    picture: CurencyV4,
    modalDescription: `In my role, I was extensively engaged in digital design projects aimed at elevating our brand identity consistently across various platforms. A primary focus of my work was to ensure a uniform and appealing user experience, regardless of the device or browser being used. This involved implementing responsive design principles and conducting rigorous compatibility testing to guarantee that our digital assets looked and functioned flawlessly across a wide array of devices, screen sizes, and browsers.

A significant aspect of my contribution was centered around the development of modern and interactive web components. To achieve this, I employed a combination of JavaScript, HTML5, and SCSS (Sass) to create engaging and functional UI elements. These components not only enhanced the visual appeal of our digital interfaces but also played a crucial role in providing users with intuitive and seamless interactions.

Collaboration was a key element of my work process. I actively engaged with stakeholders from various teams, including design, development, and marketing, to ensure the efficient distribution and integration of UI components. By working closely with these stakeholders, I was able to gather valuable feedback and insights that contributed to the refinement and optimization of our digital design efforts.

In addition to my technical contributions, I also played a role in maintaining and evolving our brand identity guidelines. This involved adhering to established design principles and ensuring that our digital assets consistently reflected our brand's visual identity and values.

Overall, my role involved a blend of technical expertise, creativity, and effective communication. Through my work on digital design initiatives, I contributed to enhancing our brand's digital presence, elevating user experiences, and ensuring a cohesive and engaging visual identity across various platforms.
    `,
  },
];

export default function Body() {
  return (
    <Contents className="container">
      {Project.defaultProps.map((project) => (
        <Project {...project} />
      ))}
    </Contents>
  );
}
