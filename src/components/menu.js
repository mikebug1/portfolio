import React from "react";
import styled from "styled-components";

const Header = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1rem 0;
  height: 400px;
  align-items: center;
`;
const Logo = styled.div`
  width: 15vh;
  height: 15vh;
  background: var(--text, #fff);
  border-radius: 50%;
  margin-right: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
`;
const Name = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
}
`;
const Letters = styled.span`
  font-size: 12px;
  font-weight: 800;
  text-transform: uppercase;
  color: var(--text, #fff);
`;
const M = styled.div`
  text-align: center;
  animation-name: spin, depth;
  animation-timing-function: linear;
  animation-iteration-count: infinite;
  animation-duration: 20s;
  font-family: Monomaniac One;
  @keyframes spin {
    from {
      transform: rotateY(0deg);
    }
    to {
      transform: rotateY(-360deg);
    }
  }
  @keyframes depth {
    0% {
      text-shadow: 0 0 black;
    }
    25% {
      text-shadow: 1px 0 black, 2px 0 black, 3px 0 black, 4px 0 black,
        5px 0 black;
    }
    50% {
      text-shadow: 0 0 black;
    }
    75% {
      text-shadow: -1px 0 black, -2px 0 black, -3px 0 black, -4px 0 black,
        -5px 0 black;
    }
    100% {
      text-shadow: 0 0 black;
    }
  }
`;
const LogoContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-start;
  gap: 1rem;
`;

const Contacts = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  font-size: 12px;
  font-weight: 200;
  width: 200px;
  font-family: "Roboto", sans-serif;
  width: 100%;
  max-width: none;
  a {
    text-decoration: none;
    color: var(--text, #ffa75d);
  }
  color: var(--text, #ffa75d);
  :link {
    color: var(--text, #fff);
  }
  :hover {
    animation: color-change 6s infinite;
  }
  :active {
    color: var(--text, #fff);
  }
  :visited {
    color: var(--text, #hjh);
  }
  @keyframes color-change {
    0% {
      color: red;
    }

    28% {
      color: yellow;
    }
    42% {
      color: green;
    }
    57% {
      color: blue;
    }
    71% {
      color: indigo;
    }
    85% {
      color: violet;
    }
    100% {
      color: red;
    }
  }
`;

export default function Menu() {
  return (
    <Header
      className="container"
      style={{
        padding: "0 1rem",
      }}
    >
      <LogoContainer>
        <Logo>
          <M>
            <svg
              width="120"
              height="112"
              viewBox="0 0 60 56"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M2 18.3794C4.48495 15.362 6.16788 11.9211 8.99433 9.10864C11.6055 6.51038 14.5678 4.54591 17.9792 3.17006C18.6574 2.89655 21.318 1.56083 22.2132 2.14731C22.7933 2.52742 22.4356 4.01532 22.3671 4.43476C21.8651 7.51265 20.9037 10.5657 19.6838 13.4306C16.4231 21.0883 11.6309 28.2594 7.34472 35.3814C6.71001 36.436 6.02243 37.4826 5.44218 38.5706C4.85521 39.6712 5.54873 38.3308 5.68412 37.9987C8.31485 31.5472 12.1826 25.3399 16.1096 19.6111C17.9319 16.9528 19.9382 14.4198 22.0152 11.957C23.4064 10.3074 24.8697 8.55312 26.7001 7.36006C28.2813 6.32943 28.343 9.60831 28.2837 10.4173C27.9044 15.5918 25.7444 20.5332 23.4229 25.0988C22.3498 27.2092 19.2016 33.5716 20.1457 31.4003C22.0318 27.0622 24.8262 23.0469 27.7338 19.3472C30.2877 16.0977 32.9477 13.287 36.6637 11.3851C37.218 11.1014 38.1697 10.4033 38.2143 11.3851C38.4689 16.9854 34.8192 22.4964 32.0998 27.0784C29.3562 31.7012 26.7978 36.3721 24.7866 41.3639C24.5343 41.99 24.3291 42.7346 23.9727 43.3215C23.6933 43.7817 23.9985 42.2329 24.1487 41.7159C25.8747 35.7739 29.9808 30.1392 34.6072 26.1326C37.2098 23.8786 40.2317 21.8154 43.4491 20.5459C44.8929 19.9762 46.3182 19.7218 47.7601 20.4139C49.6166 21.3051 50.2595 23.9433 50.2125 25.8576C50.0948 30.6519 44.5032 33.0406 40.7327 34.4136C39.0945 35.0102 34.0554 36.1371 35.575 35.2824C36.5936 34.7094 38.419 34.9142 39.468 35.0185C42.5175 35.3216 45.3818 36.4065 48.156 37.6578C50.6008 38.7607 53.5223 40.465 50.8943 43.1455C47.6341 46.4709 42.6591 48.1185 38.2033 49.0181C34.0954 49.8475 29.6977 49.8338 25.6114 48.8641C24.6126 48.6271 22.5937 47.7501 24.2037 46.6097C24.6579 46.288 25.3644 46.2011 25.8863 46.1368C28.2942 45.8403 30.815 45.8313 33.2325 45.9828C36.0829 46.1615 39.4023 46.8951 40.9527 49.524C41.2342 50.0014 41.6754 50.5683 41.7775 51.1296C41.8494 51.5251 41.6445 52.0691 41.8435 52.4273C42.1049 52.8979 43.3031 53.0768 43.658 53.1531C48.3269 54.1572 53.1584 54.2335 57.9106 54.3188"
                stroke="black"
                stroke-width="3"
                stroke-linecap="round"
              />
            </svg>
          </M>
        </Logo>
        <Name>
          <Letters>Mike Buglione</Letters>
          <Letters>Software Engineer/Designer</Letters>
        </Name>
        <Contacts>
          <a
            href="https://www.linkedin.com/in/michael-buglione-913971129/"
            target="_blank"
            rel="noopener noreferrer"
          >
            LinkedIn
          </a>
          <a
            href="https://GitHub.com/mikebug"
            target="_blank"
            rel="noopener noreferrer"
          >
            GitHub
          </a>
          <a
            href="https://michaelbuglione.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Website
          </a>
        </Contacts>
      </LogoContainer>
    </Header>
  );
}
